<?php

if (!isset($_SESSION)) {
    session_start();
}

?>

<?php
if (isset($_SESSION['id'])) {
    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">


        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/css/about.css">
        <link rel="stylesheet" href="./assets/css/navbar.css">
        <link rel="stylesheet" href="./assets/css/footer.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <title>A PROPOS</title>
    </head>

    <body>

        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>
        </div>


        <section id="about">
            <div class="container">
                <div class="row justify-content-center offset-lg-2">
                    <h1> À propos de nous </h1>
                </div>
                <div class="row justify-content-center mt-4 mb-4 offset-lg-2">
                    <div class="col-lg-4 col-sm-6 col-xs-4 align-self-center mr-3 ">
                        <img src="./assets/img/europe.jpg" class="img-fluid europe " alt="EUROPE">
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-4 align-self-center">

                        <p><span style="color : #A58701;">
                                <i class="fas fa-quote-left fa-4x"></i>
                            </span>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem animi, harum facilis odio voluptates beatae laudantium impedit consequatur saepe placeat similique a hic officiis repellendus ducimus velit! Voluptatem, tempore iure?7
                        </p>

                    </div>
                </div>
                <div class="row justify-content-center mt-4 mb-4 offset-lg-2 ">

                    <div class="col-lg-4 col-sm-6 align-self-center mr-3">
                        <p><span style="color : #A58701;">
                                <i class="fas fa-quote-left fa-4x"></i>
                            </span>
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem animi, harum facilis odio voluptates beatae laudantium impedit consequatur saepe placeat similique a hic officiis repellendus ducimus velit! Voluptatem, tempore iure?7
                        </p>
                    </div>
                    <div class="col-lg-4 col-sm-6  align-self-center">
                        <img src="./assets/img/europeengage.jpeg" class="img-fluid europe" alt="EUROPE">
                    </div>
                </div>

                <div class="row justify-content-center offset-lg-2 mb-4 mt-5">
                    <h3>Les membres et administrateurs du groupement</h3>
                </div>



                <div class="row justify-content-center offset-lg-2">
                    <div class="col-lg-6 col-sm-6 mb-5  ">


                        <ul class="list_style">
                            <li>Grégory LELONG, Vice-Président, élu membre représentant de
                                Communauté d’Agglomération Valenciennes Métropole</li>
                            <li>Dalila DUWEZ GUESMIA, Vice-Présidente, élue membre représentante de
                                la Commanauté d’Agglomération de La Porte du Hainaut</li>
                            <li>Christian BISIAUX, élu membre représentant de la Communauté
                                d’Agglomération Valenciennes Métropole </li>
                            <li>Zahra GUEZZOU, élue membre représentant de Réussir en Sambre-
                                Avesnois </li>
                            <li>Michel QUIEVY, élu membre représentant de la Communauté
                                d’Agglomération de La Porte du Hainaut
                                L’agent comptable public est invité
                                d’Administration.
                                la
                                à participer à chaque Conseil</li>
                        </ul>
                        </p>

                    </div>
                    <div class="col-lg-6 col-sm-6 align-self-center ">
                        <img src="./assets/img/organigramme_gip.png" alt="ORGANIGRAMME" class="img-fluid europe">
                    </div>
                </div>
            </div>
        </section>

        <div class="container include">
            <div class="row">
                <?php include('footer.php'); ?>
            </div>
        </div>

        <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>

    </body>

    </html>

<?php

} else {

    header('location: index.php');

    exit;
}
?>