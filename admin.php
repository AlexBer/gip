<?php

if (!isset($_SESSION)) {
    session_start();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/login.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Administration GIP</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>

    <section id="login" class="offset-lg-2">
        <div class="container">
            <div>
                <form method="GET">
            </div>
            <div class="row justify-content-center">
                <h2>Donner les droits d'aministrateur à un membre</h2>
            </div>
            <div class="row justify-content-center">
                <input type="text" name="q" placeholder="Recherche d'un membre...">
                <button type="submit" id="sendForm" class="btn btn-primary">Rechercher</button>
            </div>
            </form>

            <div class="row justify-content-center">
                <h2>Créer une newsletter</h2>
            </div>
            <div>
                <form action="newsletter.php" method="post">
            </div>
            <div class="row justify-content-center">
                <input type="text" placeholder="Titre de la newsletter" name="title">
            </div>
            <div class="row justify-content-center">
                <textarea placeholder="Message de la newsletter" name="message"></textarea>
            </div>
            <div class="row justify-content-center">
                <button type="submit" id="sendForm" class="btn btn-primary">Envoyer</button>
            </div>
            </form>
            <div class="row justify-content-center">
                <h2>Ajouter une catégorie de projet</h2>
            </div>
            <div>
                <form action="createcategorie.php" method="post">
            </div>
            <div class="row justify-content-center">
                <input type="text" name="name" placeholder="Nom de la catégorie">
            </div>
            <div class="row justify-content-center">
                <button type="submit" id="sendForm" class="btn btn-primary">Ajouter</button>
            </div>
            </form>
            <div class="row justify-content-center">
                <h2>Supprimer des catégories</h2>
            </div>
            <div>
                <form action="deletecategories.php" method="POST">
            </div>
            <?php
            $listofcategories = getCategories();
            while ($data = $listofcategories->fetch()) {
                echo '<div class="row justify-content-center">' . $data['name'] . '<input type="checkbox" name="' . $data['id'] . '" value="' . $data['id'] . '"></div>';
            };
            ?>
            <div class="row justify-content-center">
                <button type="submit" id="sendForm" class="btn btn-primary">Supprimer</button>
            </div>
            </form>
            <?php
            if (isset($_GET['q']) and !empty($_GET['q'])) {
                $q = htmlspecialchars($_GET['q']);
                $connexion = getConnexion();
                $response = $connexion->query('SELECT * FROM users WHERE name LIKE "%' . $q . '%"');



                if ($response->rowCount() > 0) {
                    while ($data = $response->fetch()) {
                        echo '<div class="row justify-content-center mt-5">';
                        echo "<ul><li>Nom : " . $data['name'] ."</li>";
                        echo "<li> Prénom : " . $data['firstname'] . "</li>";
                        echo "<li> Société : " . $data['society_name'] . "</li></<li>";
                        echo '</div>';
                        ?> .
                        <div>
                        <form method="POST" action="result_role.php">
                        </div>
                            <div class="row justify-content-center mt-2">
                                <div class="col-lg-8">
                                    <label for="email">Adresse E-mail</label>
                                    <input required type="email" class="form-control" id="email" name="email" value="<?php echo $data['email']; ?>">
                                    </div>
                                </div>
                                    <div class="row justify-content-center mt-2">
                                        <div class="col-lg-8">
                                    <label for="admin">Rôle du membre</label>
                                    <select required class="form-control" id="admin" name="admin">
                                        <option <?php if ($data['admin'] == 0) {
                                                    echo "selected";
                                                } ?>value="0">Utilisateur</option>
                                        <option <?php if ($data['admin'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Administrateur</option>
                                    </select>
                                    </div>
                                    </div>
                                <div class="row justify-content-center">
                                    <button type="submit" id="sendForm" class="btn btn-primary center">Modifier les droits</button>
                                </div>
                        </form>

                    <?php
                }
            } else {
                echo "Aucun résultat pour: $q";
            }
        }

        ?>
        </div>
    </section>
    <?php include('footer.php');