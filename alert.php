<?php

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_SESSION['id'])){
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/alert.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Lancement d'alerte</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>

    <div class="container">
        <div class="row offset-lg-2 mb-5 mt-5">
            <div class=" col-lg-12">
                <h1 class="title_alert">Signalement de fraude ou de conflit d’intérêt – lanceur d’alerte</h1>
                <hr>
            </div>
        </div>
        <div class="row offset-lg-2">
            <div class="col-lg-10">
            <p>Tout signalement de fraude ou situation de conflit d’intérêt peut être signalé à l’adresse : lanceur.alertes@hautsdefrance.fr</p>
            </div>
        </div>
        <div class="row offset-lg-2">
            <div class="col-lg-10">
            <p>Le lanceur d’alerte bénéficie de la protection organisée par l’article 25 de la loi N° 2013-907 du 11 octobre 2013 relative à la transparence de la vie publique :</p>
            </div>
        </div>
        <div class="row offset-lg-2">
            <div class="col-lg-10">
            <p>« I. ― Aucune personne ne peut être écartée d’une procédure de recrutement ou de l’accès à un stage ou à une période de formation professionnelle, ni être sanctionnée, licenciée ou faire l’objet d’une mesure discriminatoire, directe ou indirecte, notamment en matière de rémunération, de traitement, de formation, de reclassement, d’affectation, de qualification, de classification, de promotion professionnelle, de mutation ou de renouvellement de contrat, pour avoir relaté ou témoigné, de bonne foi, à son employeur, à l’autorité chargée de la déontologie au sein de l’organisme, à une association de lutte contre la corruption agréée en application du II de l’article 20 de la présente loi ou de l’article 2-23 du code de procédure pénale ou aux autorités judiciaires ou administratives de faits relatifs à une situation de conflit d’intérêts, telle que définie à l’article 2 de la présente loi, concernant l’une des personnes mentionnées aux articles 4 et 11, dont elle aurait eu connaissance dans l’exercice de ses fonctions.
Toute rupture du contrat de travail qui en résulterait ou tout acte contraire est nul de plein droit.
En cas de litige relatif à l’application des deux premiers alinéas du présent I, dès lors que la personne établit des faits qui permettent de présumer qu’elle a relaté ou témoigné, de bonne foi, de faits relatifs à une situation de conflit d’intérêts, il incombe à la partie défenderesse, au vu de ces faits, de prouver que sa décision est justifiée par des éléments objectifs étrangers à la déclaration ou au témoignage de la personne intéressée. Le juge peut ordonner toute mesure d’instruction utile. »</p>
            </div>
        </div>
        <div class="row offset-lg-2">
            <div class="col-lg-10">
            <p>Par ailleurs, l’agent public assurant la fonction de lanceur d’alertes bénéficiera de protections organisées comme suit :</p>
            <ul class="list_alert">
                <li>Les décisions de poursuites ou de signalement sont prises dans le cadre du comité en charge des alertes déontologiques.</li>
                <li>L’agent a droit à la protection fonctionnelle au titre de sa fonction de lanceur d’alertes.</li>
            </ul>
            </div>
        </div>
        <div class="row offset-lg-2 mb-5">
            <div class="col-lg-10">
            <p>Obligations du lanceur d’alertes :</p>
            <ul class="list_alert">
                <li>Garantir aux agents publics titulaires comme non titulaires de droit public signalant des risques de fraude ou de conflits d’intérêt, l’anonymat de leurs déclarations.</li>
                <li>Garantir aux tiers signalant des risques de fraude ou de conflits d’intérêt, l’anonymat de leurs déclarations.</li>
                <li>Saisir le comité d’appui à la prise en charge des signalements, de toutes alertes émanant des services ou de tiers.</li>
                <li>Informer son autorité de rattachement des alertes et des décisions du comité.</li>
                <li>Gérer l’ensemble des suites et/ou poursuites judiciaires décidées dans le cadre du comité dans la limite des attributions du lanceur d’alerte.</li>
            </ul>
            </div>
        </div>
</div>

        <div class="container include">
            <div class="row">
                <?php include('footer.php'); ?>
            </div>
        </div>

        <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
<?php 
}else{
    header('location: index.php');

    exit;
}