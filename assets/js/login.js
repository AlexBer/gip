document.getElementById('email').addEventListener('blur', function () {
    var email = document.getElementById("email").value;
    ajaxGet("api/verifyemail.php?email=" + email, function (reponse) {
        var exist = JSON.parse(reponse)

        if (exist == 'false') {
            document.getElementById('sendForm').disabled = true
            document.getElementById('errorNoAccount').style.display = "none"
            document.getElementById('errorConfirm').style.display = "block"

        } else if (exist == 'true') {
            document.getElementById('sendForm').disabled = false
            document.getElementById('errorNoAccount').style.display = "none"
            document.getElementById('errorConfirm').style.display = "none"
        } else {
            console.log("fonction else")
            document.getElementById('sendForm').disabled = true
            document.getElementById('errorConfirm').style.display = "none"
            document.getElementById('errorNoAccount').style.display = "block"
        }
    });
})