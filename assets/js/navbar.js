document.getElementById('showMenu').addEventListener('click', function () {
    document.getElementById('sidebar').style.display = "block"
    document.getElementById('showMenu').style.display = "none"
    document.getElementById('arrow').style.display = "block"
});

document.getElementById('hideMenu').addEventListener('click', function () {
    document.getElementById('sidebar').style.display = "none"
    document.getElementById('showMenu').style.display = "block"
    document.getElementById('arrow').style.display = "none"
});