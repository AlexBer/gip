document.getElementById('cemail').addEventListener('blur', function () {
    var email = document.getElementById('email').value
    var cemail = document.getElementById('cemail').value

    if (email != cemail) {
        document.getElementById('sendForm').disabled = true
        document.getElementById('emailAlert').style.display = "block"
    } else {
        document.getElementById('sendForm').disabled = false
        document.getElementById('emailAlert').style.display = "none"
    }

})
document.getElementById('cpassword').addEventListener('blur', function () {
    var password = document.getElementById('password').value
    var cpassword = document.getElementById('cpassword').value

    if (cpassword != password) {
        document.getElementById('sendForm').disabled = true
        document.getElementById('passwordAlert').style.display = "block"
    } else {
        document.getElementById('sendForm').disabled = false
        document.getElementById('passwordAlert').style.display = "none"
    }

})
document.getElementById('society_postalcode').addEventListener('blur', function () {

    var codepostal = document.getElementById("society_postalcode").value;
    ajaxGet("https://mes-aides.gouv.fr/api/outils/communes/" + codepostal, function (reponse) {

        // Transforme la réponse en un tableau de villes
        var villes = JSON.parse(reponse);

        // APPEL DE LA FONCTION RAZ
        razSelect()

        // Ajoute d'une option au select pr chaque ville du tableau

        villes.forEach(function (ville) {
            ajouterAuSelect(ville.nom)
            console.log(ville.name)
        });
    });
})

// fonction pour ajouter une ville au select
function ajouterAuSelect(ville) {
    var select = document.getElementById("society_city");
    var newOption = new Option(ville, ville)
    select.options.add(newOption);
}
// fonction pour remettre a 0 le select (en cas de nouveau code postal saisi)
function razSelect(){
    document.getElementById("society_city").innerHTML = ''
}

document.getElementById('email').addEventListener('blur', function () {

    var email = document.getElementById("email").value;
    ajaxGet("api/email.php?email=" + email, function (reponse) {

        var exist = JSON.parse(reponse);
        console.log(exist)
        if(exist == 'false'){
            document.getElementById('emailexist').style.display = "none"
            document.getElementById('sendForm').disabled = "true"
        }
        else if(exist == 'true'){
            document.getElementById('emailexist').style.display = "block"
            document.getElementById('sendForm').disabled = "false"
        }
    });
})