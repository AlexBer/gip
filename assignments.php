<?php
if (!isset($_SESSION)) {
  session_start();
}
?>
<?php
if (isset($_SESSION['id'])){
  ?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/assignments.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Missions GIP</title>
  </head>

  <body>
    <div class="container include">
      <div class="row">
        <?php include('navbar.php'); ?>
      </div>
    </div>
    <section id="assignments">
      <div class="container col-lg-10">
        <div class="row offset-lg-2">
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/information.jpg" alt="INFORMATION">
              <div class="overlay">
                <h2>INFORMATION</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. </a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/orientation.jpg" alt="ORIENTATION">
              <div class="overlay">
                <h2>ORIENTATION</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. </a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-lg-4 offset-lg-2">
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/formations.jpg" alt="FORMATION">
              <div class="overlay">
                <h2>FORMATION</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac.</a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-12 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/assistance.jpg" alt="ASSISTANCE">
              <div class="overlay">
                <h2>ASSISTANCE</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac.</a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="presentation_assignments">
          <div class="row offset-lg-2">
            <div class="col">
              <h2> Mettre en œuvre, développer et renouveler l’offre territoriale d’insertion avec Europe en Hainaut </h2>
              <i class="fas fa-arrow-right"> Pour qui ?</i>
              <p> Les collectivités, associations, entreprises solidaires, têtes de réseau, tous acteurs intervenant sur le champ de l’inclusion sociale et professionnelle.</p>
            </div>
          </div>
          <div class="row offset-lg-2">
            <div class="col">
              <i class="fas fa-arrow-right"> Quels dispositifs d’insertion ?</i>
              <ul>
                <li> Plans locaux d’insertion </li>
                <li> Insertion par l’activité économique </li>
                <li> Actions d’Utilité Sociale (politique de la ville, initiatives citoyennes, etc.) </li>
              </ul>
            </div>
          </div>
          <div class="row offset-lg-2">
            <div class="col">
              <i class="fas fa-arrow-right"> Quels Projets ?</i>
              <p>Soutien aux démarches d’accompagnement des publics et d’ingénierie plus globale : </p>
              <ul>
                <li> Mise en œuvre et développement des parcours intégrés d’accès à l’emploi : </li>
                <p>> Accueil, accompagnement, suivi, mise en emploi </p>
                <p>> Levée de freins, redynamisation des publics, formation, qualification des publics</p>
                <p>> Optimisation des outils de mise en oeuvre des parcours</p>
                <li> Mobilisation des employeurs et des entreprises dans les parcours d’insertion :</li>
                <p>> Médiation, accès et suivi dans l’emploi</p>
                <p>> Accompagnement des entreprises dans le développement inclusif de leurs compétences</p>
                <p>> Promotion des entreprises, de leurs valeurs et leurs métiers</p>
                <p>> Coopération entre les entreprises du secteur marchand et les structures de l’IAE</p>
                <li> Coordination et animation en faveur de l’offre territoriale d’insertion et de l’ESS : </li>
                <p>> Développement et renouvellement de l’offre territoriale d’insertion</p>
                <p>> Pilotage de dispositif territorial d’insertion </p>
              </ul>
            </div>
          </div>
          <div class="row offset-lg-2">
            <div class="col">
              <i class="fas fa-arrow-right"> Quand ?</i>
              <ul>
                <li> Permanent pour la période 2018-2020 </li>
                <li> Dépôt des projets annuels : avant le 30 septembre </li>
                <li> Création et envoi du dossier sur le lien suivant </li>
              </ul>
            </div>
          </div>
          <div class="row offset-lg-2">
            <div class="col">
              <i class="fas fa-arrow-right"> Quel soutien financier ?</i>
              <ul>
                <li> Soutien du Fonds Social Européen en cofinancement des contreparties nationales déjà mobilisées sur le projet </li>
                <li> Enveloppe globale prévisionnelle de 8,7 M€ avec un taux d’intervention moyen de 60% sur les projets </li>
              </ul>
            </div>
          </div>
          <div class="row offset-lg-2">
            <div class="col">
              <i class="fas fa-arrow-right"> Quelle priorisation thématique ?</i>
              <p> L’innovation : </p>
              <ul>
                <li> Adaptation du mode de captation et des modalités de mobilisation des publics ; </li>
                <li> Mobilisation de l’ESS ; </li>
                <li> Rationalisation des outils d’insertion territoriaux ; </li>
                <li> Ingénierie et innovation sociale ; </li>
                <li> Solutions innovantes de coordination et d’échange d’informations entre acteurs. </li>
                <li> Labellisation et bonification de financement lors de la sélection du projet : </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="partenaires offset-lg-2">
          <img src="./assets/img/partenariat.png" class="rounded mx-auto d-block img-fluid" alt="...">
        </div>
      </div>

    </section>
    <div class="container include">
      <div class="row">
        <?php include('footer.php'); ?>
      </div>
    </div>

    <script type="text/javascript" src="./assets/js/script.js"></script>

  </body>

  </html>
<?php
} else {
  header('location: index.php');
  exit;
}
?>