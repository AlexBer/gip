<?php

function getConnexion(){
  $PARAM_hote='localhost'; // le chemin vers le serveur
  $PARAM_port='3306';
  $PARAM_nom_bd='gip'; // le nom de votre base de données
  $PARAM_utilisateur='alexber'; // nom d'utilisateur pour se connecter
  $PARAM_mot_passe='popschool'; // mot de passe de l'utilisateur pour se connecter


  try {
    
    $connexion = new PDO('mysql:host='.$PARAM_hote.';port='.$PARAM_port.';dbname='.$PARAM_nom_bd, $PARAM_utilisateur, $PARAM_mot_passe, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
  
    $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    return $connexion;
    
  }

  catch(Exception $e) {
    echo 'Erreur : '.$e->getMessage().'<br />';
    echo 'N° : '.$e->getCode();
  }
}

?>