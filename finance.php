<?php
if (!isset($_SESSION)) {
    session_start();
}

if (isset($_SESSION['id'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
        <link rel="stylesheet" href="./assets/css/navbar.css">
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/style.css">
        <link rel="stylesheet" href="./assets/css/footer.css">
        <link rel="stylesheet" href="./assets/css/financez.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link href="./assets/fonts/GOTHAM">
        <title>Europe en Hainaut</title>
    </head>

    <body>
        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>

            <section>
                <div class="container">
                    <div class="row offset-lg-2">
                        <div class="table-responsive">
                            <table class="table justify-content-center">
                                <h2>APPELS A CANDIDATURE / PROJET</h2>
                                <thead>
                                    <tr>
                                        <th scope="col justify-content-center">Programme</th>
                                        <th scope="col justify-content-center">Projet</th>
                                        <th scope="col justify-content-center">Date limite</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $req = getProjects();
                                    while ($data = $req->fetch()) {; ?>
                                        <tr>
                                            <td><a href="./projectdetail.php?id=<?php echo $data['id']; ?>"><?php echo $data['program']; ?></a></td>
                                            <td><?php echo $data['name']; ?></td>
                                            <td><?php if (isset($data['deadline'])) {
                                                    echo $data['deadline'];
                                                } else {
                                                    echo "Non défini";
                                                } ?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </section>


            <div class="container include">
                <div class="row">
                    <?php include('footer.php'); ?>
                </div>
            </div>
            <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="./assets/js/navbar.js"></script>
    </body>

    </html>

<?php

} else {
    header('location: index.php');
    exit;
}

?>