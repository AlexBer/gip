<?php

require('connexionbdd.php');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

function sendMail($token, $email)
{

    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'projetservdrone@gmail.com';            // SMTP username
        $mail->Password   = 'projetx59';                            // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('projetservdrone@gmail.com', 'Europe en Hainaut');
        $mail->addAddress($email);
        $mail->isHTML(true);
        $mail->Subject = "Confirmation d'inscription";
        $mail->Body    = '
<head>
    <title>Rating Reminder</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width" name="viewport">
    </head>
    <body style="background-color: #f4f4f5;">
    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
    <tbody><tr>
    <td style="text-align: center;">
    <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%; max-width: 680px; height: 100%;">
    <tbody><tr>
    <td>
    <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
    <tbody><tr>
    <td style="padding-top: 24px;">
    <img src="https://image.noelshack.com/fichiers/2019/22/4/1559246470-logo-nb.png" style="width: 325px; height:175px;">
    </td>
    </tr>
    <tr>
    <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">Confirmez votre compte</td>
    </tr>
    <tr>
    <td style="padding-top: 48px; padding-bottom: 48px;">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tbody><tr>
    <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    <td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
    Bienvenue sur le site GIP Europe en Hainaut
    </td>
    </tr>
    <tr>
    <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
    Cliquez sur le bouton pour valider votre compte
    </td>
    </tr>
    <tr>
    <td>
    <a data-click-track-id="37" href="localhost/gip/confirm_account.php?token=' . $token . '&email=' . $email . '" style="margin-top: 36px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #ffffff; font-size: 12px; font-style: normal; font-weight: 600; letter-spacing: 0.7px; line-height: 48px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 220px; background-color: #0e537a; border-radius: 28px; display: block; text-align: center; text-transform: uppercase" target="_blank">
    Valider le compte
    </a>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    </tr>
    <tr>
    <td style="height: 72px;"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </body>
';
        $mail->send();
        echo 'Message envoyé. Vérifiez le dossier SPAM.';
    } catch (Exception $e) {
        echo "Le message n'a pas pu être envoyé. Erreur: {$mail->ErrorInfo}";
    }
}

function confirmAccount($token, $email)
{
    $connexion = getConnexion();
    $data = $connexion->query(" SELECT confirmation_tkn FROM users WHERE email = '$email' ");
    $donnees = $data->fetch();
    // var_dump( $donnees);
    $tkn = $donnees['confirmation_tkn'];
    echo $tkn . '<br/>' . $token;
    if ($tkn === $token) {
        $connexion->query(" UPDATE users SET confirmation_tkn = 'confirmed' WHERE email = '$email' ");
    } else { }
}

function getProjects()
{
    $connexion = getConnexion();
    $req = $connexion->query("SELECT * FROM projects");
    return $req;
}
function getProjectById($id)
{
    $connexion = getConnexion();
    $req = $connexion->query("SELECT * FROM projects WHERE id = '$id' ");
    return $req;
}
function deleteProject($id)
{
    $connexion = getConnexion();
    $connexion->query(" DELETE FROM projects WHERE id ='$id' ");
}
function editproject($id, $name, $program, $description, $specifications, $deadline)
{
    $connexion = getConnexion();
    $connexion->query("
    UPDATE projects SET name = '$name', program = '$program', description = '$description',
    specifications = '$specifications', deadline = '$deadline' WHERE id = '$id' 
    ");
}
function getUserByID($id)
{
    $connexion = getConnexion();
    $req = $connexion->query(" SELECT * FROM users WHERE id = '$id' ");
    $data = $req->fetch();
    return $data;
}

// RESET PW
function updateUserPassword($password, $email)
{
    $connexion = getConnexion();
    $connexion->query("UPDATE users SET password = '$password', reset_pw = 'undefined' WHERE email = '$email' ");
}


function sendToken($token, $email)
{

    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 1;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'projetservdrone@gmail.com';            // SMTP username
        $mail->Password   = 'projetx59';                            // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('projetservdrone@gmail.com', 'Europe en Hainaut');
        $mail->addAddress($email);
        $mail->isHTML(true);
        $mail->Subject = "Mot de passe oublié";
        $mail->Body    = '
<head>
    <title>Rating Reminder</title>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
    <meta content="width=device-width" name="viewport">
    </head>
    <body style="background-color: #f4f4f5;">
    <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
    <tbody><tr>
    <td style="text-align: center;">
    <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%; max-width: 680px; height: 100%;">
    <tbody><tr>
    <td>
    <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
    <tbody><tr>
    <td style="padding-top: 24px;">
    <img src="https://image.noelshack.com/fichiers/2019/22/4/1559246470-logo-nb.png" style="width: 325px; height:175px;">
    </td>
    </tr>
    <tr>
    <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">Confirmez votre compte</td>
    </tr>
    <tr>
    <td style="padding-top: 48px; padding-bottom: 48px;">
    <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tbody><tr>
    <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    <td style="-ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-size: 16px; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
    Bienvenue sur le site GIP Europe en Hainaut
    </td>
    </tr>
    <tr>
    <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
    Cliquez sur le bouton pour définir
    </td>
    </tr>
    <tr>
    <td>
    <a data-click-track-id="37" href="localhost/gip/confirm_account.php?token=' . $token . '&email=' . $email . '" style="margin-top: 36px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #ffffff; font-size: 12px; font-style: normal; font-weight: 600; letter-spacing: 0.7px; line-height: 48px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 220px; background-color: #0e537a; border-radius: 28px; display: block; text-align: center; text-transform: uppercase" target="_blank">
    Nouveau mot de passe
    </a>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    <tr>
    </tr>
    <tr>
    <td style="height: 72px;"></td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </td>
    </tr>
    </tbody></table>
    </body>
';
        $mail->send();
        echo 'Message envoyé. Vérifiez le dossier SPAM.';
    } catch (Exception $e) {
        echo "Le message n'a pas pu être envoyé. Erreur: {$mail->ErrorInfo}";
    }
}
function skip_accents($str, $charset)
{

    $str = htmlentities($str, ENT_NOQUOTES, $charset);

    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str);
    $str = preg_replace('#&[^;]+;#', '', $str);

    return $str;
}
function sendNewsLetter($email, $title, $message)
{
    $mailtitle = skip_accents($title, 'utf-8');

    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'projetservdrone@gmail.com';            // SMTP username
        $mail->Password   = 'projetx59';                            // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('projetservdrone@gmail.com', 'Europe en Hainaut');
        $mail->addAddress($email);
        $mail->isHTML(true);
        $mail->Subject = "$mailtitle";
        $mail->Body    =
            '<head>
        <title>' . $title . '</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="width=device-width" name="viewport">
        </head>
        <body style="background-color: #f4f4f5;">
        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
        <tbody><tr>
        <td style="text-align: center;">
        <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%; max-width: 680px; height: 100%;">
        <tbody><tr>
        <td>
        <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
        <tbody><tr>
        <td style="padding-top: 24px;">
        <img src="https://image.noelshack.com/fichiers/2019/22/4/1559246470-logo-nb.png" style="width: 325px; height:175px;">
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">' . $title . '</td>
        </tr>
        <tr>
        <td style="padding-top: 48px; padding-bottom: 48px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tbody><tr>
        <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr>
        </tr>
        <tr>
        <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
        ' . $message . '
        </td>
        </tr>
        <tr>
        <td>
        <a data-click-track-id="37" href="localhost/gip/unsubscribe.php?email=' . $email . '" style="margin-top: 36px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #ffffff; font-size: 12px; font-style: normal; font-weight: 600; letter-spacing: 0.7px; line-height: 48px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 220px; background-color: #0e537a; border-radius: 28px; display: block; text-align: center; text-transform: uppercase" target="_blank">
        Se désabonner
        </a>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr>
        </tr>
        <tr>
        <td style="height: 72px;"></td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </body>
    ';
        $mail->send();
        // echo 'Message envoyé. Vérifiez le dossier SPAM.';
        header('location: admin.php');
        exit;
    } catch (Exception $e) {
        echo "Le message n'a pas pu être envoyé. Erreur: {$mail->ErrorInfo}";
    }
}
function unsubscribe($email)
{
    $connexion = getConnexion();
    $connexion->query("UPDATE users SET newsletter = '0' WHERE email = '$email' ");
}
function getCategories()
{
    $connexion = getConnexion();
    $req = $connexion->query(" SELECT * FROM categories WHERE isDeleted = '0' ");
    return $req;
}
function deleteCategories($id)
{
    $connexion = getConnexion();
    $connexion->query(" UPDATE categories SET isDeleted = '1' WHERE id = '$id' ");
}
function subscrireToCategorie($categorieid, $userid)
{
    $connexion = getConnexion();
    $connexion->query(" INSERT INTO subscription (user_id, categorie_id) VALUES ('$userid', '$categorieid') ");
}
function cleanSubscriptions($id)
{
    $connexion =  getConnexion();
    $connexion->query(" DELETE from subscription WHERE user_id = '$id' ");
}
function getMailsOfSubscribers($categories, $project_id)
{

    // Initialisation tableau vide pour récupérer les emails des membres
    $array = [];

    $connexion = getConnexion();

    // Boucle dans les categories sélectionnées a la création du projet afin de récupéré les emails de membres abonnés à ces catégories
    foreach ($categories as $categorie) {
        // Récupération des emails des membres
        $req = $connexion->query(" SELECT email FROM users INNER JOIN subscription WHERE categorie_id = '$categorie' ");
        $e = $req->fetch();
        // Mets les emails dans le tableau $array
        array_push($array, $e['email']);
    }
    // Enlève les doublons dans le tableau des emails (Si abonnés a 2 catégories du projet) -> stocké dans nouveau tableau $emails
    $emails = array_unique($array);
    var_dump($emails);
    foreach ($emails as $email) {
    sendMailToSubscribers($email, $project_id);
    }
}
function sendMailToSubscribers($email, $project_id){
    $data = getProjectById($project_id);
    $project = $data->fetch();
    $mail = new PHPMailer(true);
    try {
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'projetservdrone@gmail.com';            // SMTP username
        $mail->Password   = 'projetx59';                            // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('projetservdrone@gmail.com', 'Europe en Hainaut');
        $mail->addAddress($email);
        $mail->isHTML(true);
        $mail->Subject = "Nouveau projet dans vos abonnements : ".$project['name'];
        $mail->Body    =
            '<head>
        <title>Nouveau projet disponible dans vos abonnements</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="width=device-width" name="viewport">
        </head>
        <body style="background-color: #f4f4f5;">
        <table cellpadding="0" cellspacing="0" style="width: 100%; height: 100%; background-color: #f4f4f5; text-align: center;">
        <tbody><tr>
        <td style="text-align: center;">
        <table align="center" cellpadding="0" cellspacing="0" id="body" style="background-color: #fff; width: 100%; max-width: 680px; height: 100%;">
        <tbody><tr>
        <td>
        <table align="center" cellpadding="0" cellspacing="0" class="page-center" style="text-align: left; padding-bottom: 88px; width: 100%; padding-left: 120px; padding-right: 120px;">
        <tbody><tr>
        <td style="padding-top: 24px;">
        <img src="https://image.noelshack.com/fichiers/2019/22/4/1559246470-logo-nb.png" style="width: 325px; height:175px;">
        </td>
        </tr>
        <tr>
        <td colspan="2" style="padding-top: 72px; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #000000; font-size: 48px; font-smoothing: always; font-style: normal; font-weight: 600; letter-spacing: -2.6px; line-height: 52px; mso-line-height-rule: exactly; text-decoration: none;">' . $project['name']. '</td>
        </tr>
        <tr>
        <td style="padding-top: 48px; padding-bottom: 48px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%">
        <tbody><tr>
        <td style="width: 100%; height: 1px; max-height: 1px; background-color: #d9dbe0; opacity: 0.81"></td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr>
        </tr>
        <tr>
        <td style="padding-top: 24px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #9095a2; font-style: normal; font-weight: 400; letter-spacing: -0.18px; line-height: 24px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 100%;">
        ' . $project['description'] . '
        </td>
        </tr>
        <tr>
        <td>
        <a data-click-track-id="37" href="http://localhost/gip/projectdetail.php?id=' . $project['id']. '" style="margin-top: 36px; -ms-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: 100%; color: #ffffff; font-size: 12px; font-style: normal; font-weight: 600; letter-spacing: 0.7px; line-height: 48px; mso-line-height-rule: exactly; text-decoration: none; vertical-align: top; width: 220px; background-color: #0e537a; border-radius: 28px; display: block; text-align: center; text-transform: uppercase" target="_blank">
        Voir plus
        </a>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        <tr>
        </tr>
        <tr>
        <td style="height: 72px;"></td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </td>
        </tr>
        </tbody></table>
        </body>
    ';
        $mail->send();
        // echo 'Message envoyé. Vérifiez le dossier SPAM.';
        header('location: admin.php');
        exit;
    } catch (Exception $e) {
        echo "Le message n'a pas pu être envoyé. Erreur: {$mail->ErrorInfo}";
    }   
}