<html>

<head>
</head>

<body>
    <section id="footer">
        <footer class="page-footer font-small stylish-color-dark pt-4 offset-lg-2">

            <div class="footer container-fluid">
                <div class="container-fluid text-center text-md-left">
                    <div class="row">
                        <div class="col-md-4 mx-auto">
                            <h5 class="font-weight-bold text-uppercase mt-3 mb-4"><span class="or">E</span>urop<span class="or">e</span> <span class="or">e</span>n <span class="or">H</span>a<span class="or">i</span>na<span class="or">u</span>t c'est ,</h5>
                            <p>Mettre en œuvre, développer et renouveler l’offre territoriale d’insertion.</p>
                            <p>Faciliter l'accès aux Programmes Européens.</p>
                        </div>
                        <hr class="clearfix w-100 d-md-none ">
                        <hr class="clearfix w-100 d-md-none ">
                        <div class="col-md-3 mx-auto">
                            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Liens</h5>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="links_footer" href="mentions.php">Mentions Légales</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="#!">Espace partenaires</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="#!">Espace presse</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="#!">Newsletter</a>
                                </li>
                            </ul>
                        </div>
                        <hr class="clearfix w-100 d-md-none ">
                        <div class="col-md-3 mx-auto">
                            <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Liens</h5>
                            <ul class="list-unstyled">
                                <li>
                                    <a class="links_footer" href="mentions.php">Crédits</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="#!">Lexique</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="#!">Ressources</a>
                                </li>
                                <li>
                                    <a class="links_footer" href="alert.php">Lancement d'alerte</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <ul class="list-unstyled list-inline text-center py-2">
                    <li class="list-inline-item">
                        <h5 class="mb-1">Abonnez-vous gratuitement</h5>
                    </li>
                    <li class="list-inline-item">
                        <a href="#!">
                            <img class="img-partenaires" src="./assets/img/partenaires_gip.png" alt="" width="80%" height="80%" />
                        </a>
                    </li>
                </ul>
                <hr>
                <ul class="list-unstyled list-inline text-center">
                    <li class="list-inline-item">
                        <a href="https://www.facebook.com/EuropeEnHainaut/" class="btn-floating btn-fb mx-1">
                            <span style=" color: #A58701;">
                                <i class="fab fa-facebook-f fa-2x"></i>
                            </span>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="https://twitter.com/europe_france" class="btn-floating btn-tw mx-1">
                            <span style=" color: #A58701;">
                                <i class="fab fa-twitter fa-2x"></i>
                            </span>
                        </a>
                    </li>
                    <div class="footer-copyright text-center py-3">© 2019 Copyright:
                        Europe en Hainaut
                    </div>
            </div>
        </footer>
    </section>
</body>

</html>