<?php
if (!isset($_SESSION)) {
    session_start();
}
if (isset($_SESSION['id'])) {
    ?>
    <!DOCTYPE html>
    <html lang="en">


    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="./assets/css/navbar.css">
        <link rel="stylesheet" href="./assets/css/profil.css">
        <link rel="stylesheet" href="./assets/css/footer.css">
        <link rel="stylesheet" href="./assets/css/help.css">
        <title>Nos missions GIP</title>
    </head>

    <body>
        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>
        </div>
        <section class="mb-0 mt-2" id="missions">
            <div class="container margin">
                <h1 class="text-center text-uppercase">On vous aide !</h1>
                <div class="row pt-5 offset-lg-2">
                    <div class="col-lg-5 pt-5 mr-auto first">
                        <h3 class="text-uppercase text-center blue">Formation & mise en relation</h3>
                    </div>
                    <div class="col-lg-5 pt-5 mr-auto second">
                        <h3 class="text-uppercase text-center">Financement</h3>
                    </div>
                </div>
                <div class="row mt-lg-3 mt-md-0 pt-5 offset-lg-2">
                    <div class="col-lg-5 pt-5 mr-auto third">
                        <h3 class="text-uppercase text-center">Aide la gestion de projets européens</h3>
                    </div>
                    <div class="col-lg-5 pt-5 mr-auto fourth">
                        <h3 class="text-uppercase text-center">VEILLE ET APPUI
                            TECHNIQUE</h3>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <div class="container include ">
            <div class="row">
                <?php include('footer.php'); ?>
            </div>
        </div>
        <script type="text/javascript" src="./assets/js/script.js"></script>
    </body>

    </html>
<?php
} else {
    header('location: index.php');
    exit;
}
?>