<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/style.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <link rel="stylesheet" href="./assets/css/index.css">
    <link href="./assets/fonts/GOTHAM">
    <title>Europe en Hainaut</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>
    <div class="container" >
        <div class="row justify-content-center">
        <div class="col-lg8-8 offset-lg-2">
            <h1>Le facilitateur d’accès aux financements européens<br />pour vos projets</h1>
            <div class="embed-responsive embed-responsive-16by9 mt-3">
                <iframe width="500" height="300" src="https://www.youtube.com/embed/dT_c-UCPFfc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            </iframe>
        </div>
        </div>
    </div>



    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>

    <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>