<?php

if (!isset($_SESSION)) {
    session_start();
}

if (isset($_SESSION['id'])) {

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/lexique.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Lexique</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>
    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>

    <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>
<?php
}else{
    header('location: index.php');
    exit;
}