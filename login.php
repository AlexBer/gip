<?php

if (!isset($_SESSION)) {
    session_start();
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/login.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Connexion GIP</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>

    <section id="login">

        <div class="container">
            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-8 col-sm-8">
                    <small id="required" class="form-text text-muted">Les champs marqués d'un <span class="asterisk">*</span> sont obligatoires</small>
                </div>
            </div>
            <div>
                <form method="POST" action="result_login.php">
            </div>

            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-8 col-sm-8">
                    <label for="email"><span class="asterisk">*</span> Adresse Électronique (ex: nom@exemple.fr)</label>
                    <input required type="email" class="form-control" id="email" name="email" placeholder="Entrez votre email">
                </div>
            </div>
            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-8 col-sm-8">
                    <label for="password"><span class="asterisk">*</span> Mot de Passe</label>
                    <input required type="password" class="form-control" id="password" name="password" placeholder="Entrez votre mot de passe">
                </div>
            </div>
            <div class="row justify-content-center offset-lg-2 mb-4">
                <button type="submit" id="sendForm" class="btn btn-primary">ENVOYER</button>
            </div>
            <div class="row justify-content-center offset-lg-2 mb-4">
            <span class="alert alert-danger" id="errorConfirm" role="alert" style="display:none;">Veuillez confirmer votre compte</span>
                <span class="alert alert-danger" id="errorNoAccount" role="alert" style="display:none;">Vous n'avez pas de compte. <a href="register.php">Inscrivez-vous.</a> </span>
            </div>
            <div class="row justify-content-center offset-lg-2">
                <small class="form-text text-muted">> <span class="underline">Mot de passe oublié ?</span></small>
            </div>
            <div class="row justify-content-center offset-lg-2 mb-5">
                <small class="form-text text-muted"><a href="register.php"><span class="underline">Vous n'avez pas encore de compte ? Inscrivez-vous.</span></a></small>



            </div>


        </div>
        </form>
    </section>

    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>

    <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="./assets/js/ajax.js"></script>
    <script src="./assets/js/login.js"></script>

</body>

</html>