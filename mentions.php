<?php

if (!isset($_SESSION)) {
    session_start();
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/mentions.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Mentions Légales</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>

    <div class="container">
        <div class="row contentPost text-align-center offset-lg-2">
            <div class="col-md-7 offset-md-1 col-12">
                <div class="content mt-3 mb-3">
                    <h1 class="h1 postTitle mb-5">Crédits et mentions légales</h1>
                    <h3>Editeur du site</h3>
                    <p>Le Conseil régional Hauts-de-France<br />
                        151, avenue du président Hoover<br />
                        59555 LILLE CEDEX<br />
                        Tél. : 33+(0)3.74.27.00.00</p>
                    <h3><strong>L&rsquo;hébergement est assuré par :</strong></h3>
                    <p>OVH<br />
                        2 rue kellermann<br />
                        59100 Roubaix &#8211; FRANCE<br />
                        <a href="https://www.ovh.com/" target="_blank" rel="noopener">www.ovh.com</a></p>
                    <h3>Directeur de la publication</h3>
                    <p>Xavier Bertrand<br />
                        Président du Conseil régional Hauts-de-France</p>
                    <h3>Développement Technique</h3>
                    <p>Le Conseil régional Hauts-de-France<br />
                        151, avenue du président Hoover<br />
                        59555 LILLE CEDEX<br />
                        Tél. : 33+(0)3.74.27.00.00</p>
                    <h3>Conception, design et développement</h3>
                    <p>Création graphique : Conseil régional Hauts-de-France<br />
                        Développement interne entièrement basé sur du logiciel libre et utilisant le moteur de publication WordPress</p>
                    <h3>Droit d’utilisation</h3>
                    <p>Le contenu du site est mis à disposition pour un usage privé et non exclusif. Ce droit d’utilisation comprend le droit de stockage sur écran monoposte et le droit de reproduction aux conditions ci-après définies. Il interdit la récupération des bases de données.</p>
                    <h4>Copies ou reproductions</h4>
                    <p>Le site l&rsquo;Europe s&rsquo;engage en Hauts-de-France est une publication du Conseil régional. La reproduction des informations publiées est autorisée sauf à des fins commerciales et sous réserve de la mention d’origine “Hauts-de-France” ainsi que l’url de la source.<br />
                        Sont en revanche soumis à une autorisation préalable les textes portant la mention “usage réservé”. La démarche d’autorisation doit être adressée à la direction de la communication et des relations publiques du Conseil régional Hauts-de-France qui assortira son autorisation, partielle ou intégrale, d’une mention spécifique qui devra être impérativement indiquée.<br />
                        Les photographies, images fixes ou animées, ne peuvent être reproduites sans autorisation préalable et dès lors qu’un droit de copie serait accordé, la mention du copyright indiquée au demandeur par l’administrateur du site devrait impérativement figurer sur le support utilisé à cette fin.</p>
                    <h4><strong>Finalités des cookies</strong></h4>
                    <p>Cookie incluant des informations relatives à la navigation d’un visiteur.<br />
                        Un cookie ne permet pas au site l&rsquo;Europe s&rsquo;engage en Hauts-de-France de vous identifier. En revanche, il enregistre des informations relatives à la navigation de votre ordinateur sur notre site (les pages que vous avez consultées, la date et l’heure de la consultation, etc.) qu’il peut lire lors de vos visites ultérieures.</p>
                    <h4><strong>Liens et rétroliens</strong></h4>
                    <p>Les liens présents sur le site peuvent orienter le lecteur sur des sites extérieurs dont le contenu ne peut engager la responsabilité de la rédaction du site.<br />
                        La mise en place de liens vers le site l&rsquo;Europe s&rsquo;engage en Hauts-de-France n’est pas soumis à un accord préalable. Il est seulement demandé d’indiquer explicitement la mention : “L&rsquo;Europe s&rsquo;engage en Hauts-de-France” ainsi que l’url de la source. Les liens profonds sont toutefois à privilégier. Sont exclus de cette autorisation, les sites et tout support à caractère polémique, pornographique ou xénophobe ou pouvant porter atteinte à la sensibilité du plus grand nombre.</p>
                    <h3>Les images du site, les droits photographiques et droits d’auteurs</h3>
                    <h4>Crédits photographiques</h4>
                    <p>Bokalo Dominique, Bryant Nicolas, Cornu Jean-Luc, Crochez Guillaume, Dapvril Philippe, Descatoire David, Blauwblomme Frédéric, Fourdin Hugo, Frutier Philippe, Henin Teddy, Lachant Michaël, Lo Presti François, Page Thierry, Tabary David, Thibault Pierre, Watteau Emmanuel, Waeghemacker Claude, Dhote Samuel, Drouet Mathieu (Take a Sip), Despicht Olivier, Delahaye François, Leber Léandre, Bellet Sam, Boucher Fred, Danquin J.F., Dumont Pascale, Feray Jenny, Flament Anne-Sophie, Georges Emmanuel, Grouard David/TerresdePicardie, Jacquot Claude, Leber Léandre, Lefebvre Stephan, Leleu Ludovic, Perron Gérard, Raux Didier, Rosenfeld David, Sidoli Alice, Teissèdre Benjamin, Struy Cyrille, Rambaud Thierry.</p>
                    <p>&nbsp;</p>
                    <h4>Cartes du site</h4>
                    <p>Les cartes présentées sur le site font l’objet d’un copyright du Conseil régional Hauts-de-France.<br />
                        Pour tout renseignement, contactez les services de la Région Hauts-de-France.</p>
                    <div class="essb_break_scroll"></div>
                </div>
                <div class="content">
                </div>
            </div>

        </div>
    </div>

    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>

    <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>

</body>

</html>