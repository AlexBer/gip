<html>

<head>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
</head>

<?php
if (!isset($_SESSION)) {
    session_start();
} 

require('fonctions.php');
if (isset($_SESSION['id'])) {
    $user = getUserByID($_SESSION['id']);
}
?>


<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2">

            <button class="btn-primary-transparent" id="showMenu"><i class="fas fa-bars">Menu</i></button>
            <nav id="sidebar">
                <div class="row">
                    <div class="col">
                        <a href="index.php"><img class="img-logo" src="./assets/img/logos/logo_blanc.png" width="150" height="150" alt="GIP du Hainaut"></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div id="arrow" style="display:none;">
                            <button class="btn-primary-transparent" id="hideMenu"><span style="color: white;"><i class="fas fa-arrow-left"></i></span></button>
                        </div>
                        <ul class="list-unstyled components">
                            <li class="menu-items">
                                <a href="index.php" class="menu-items-link">Accueil</a>
                            </li>
                            <li class="menu-items">
                                <a href="about.php" class="menu-items-link">A propos</a>
                            </li>
                            <li class="menu-items">
                                <a href="assignments.php" class="menu-items-link">Nos missions</a>
                            </li>
                            <li class="menu-items">
                                <a href="finance.php" class="menu-items-link">Financez votre projet</a>
                            </li>
                            <li class="menu-items">
                                <a href="training.php" class="menu-items-link">Formez-vous</a>
                            </li>
                            <li class="menu-items">
                                <a href="help.php" class="menu-items-link">On vous aide !</a>
                            </li>
                            <li class="menu-items">
                                <a href="weshare.php" class="menu-items-link">We share</a>
                            </li>
                            <li class="menu-items">
                                <a href="news.php" class="menu-items-link">Actualités</a>
                            </li>
                        </ul>
                    </div>
                </div>

            </nav>
        </div>
    </div>
    <form class="form-inline md-form form-sm mt-2 form mb-2">
        <div class="row ">
            <input class="form-control mr-3 search-bar" type="text" placeholder="Recherche" aria-label="Search">
            <button style="display: none " class="btn btn-outline-success mr-0 ml-0" type="submit">Go !</button>
        </div>

        <?php

        if (!isset($_SESSION['email'])) {
            echo '

 <li><a class="menu_login" href="login.php"><i  class="far fa-user  mr-3"> Je m\'identifie </i></a></li>
';
        } else {
            echo '

  <li><a class="menu_profil" href="profil.php"><i  class="far fa-user fa-2x mr-3 ml-3"> </i></a></li>

  <li><a class="menu_logout" href="logout.php"><i  class="fas fa-times fa-2x mr-3"> </i></a></li>

  ';


            if ($_SESSION['admin'] == 1) {
                echo '
                    <li><a class="menu_admin" href="projects.php"><i class="fas fa-user-cog fa-2x "></i></a></li>
                    ';
            }
        }


        // if(!isset($_SESSION['admin']) == 1 )  {
        //     echo '
        //     <li><a class="menu_admin" href="projects.php"><i class="fas fa-user-cog fa-2x "></i></a></li>
        //     ';
        // }


        ?>
</div>
</form>




<script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="./assets/js/navbar.js"></script>