<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/news.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <link href="./assets/fonts/GOTHAM">
    <title>Actualités Europe en Hainaut</title>
</head>

<body>

    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>

    <div class="container ">
        <div class="row offset-lg-2 mt-5">

            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                    <a class="twitter-timeline" data-width="300" data-height="255" href="https://twitter.com/ENPdC?ref_src=twsrc%5Etfw">Tweets by ENPdC</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
                    </div>
                </div>
            </div>
        </div>

        <div class="row offset-lg-2 mb-5 mt-5">
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
        </div>


        <div class="row offset-lg-2 mb-5">
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-4">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">Titre de l'évènement</h2>
                        <p class="card-text">Coute description de l'évènements, le reste sera afficher sur la vue de détail de l'évènement accessible grâce au bouton.</p>
                        <a href="#" class="btn btn-outline-success">En savoir +</a>
                    </div>
                </div>
            </div>
        </div>

    </div>


    </div>



    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>

    <script type="text/javascript" src="./assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>