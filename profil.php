<?php

if (!isset($_SESSION)) {
    session_start();
}

?>

<?php
if (isset($_SESSION['id'])) {

    ?>

    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="./assets/css/navbar.css">
        <link rel="stylesheet" href="./assets/css/profil.css">
        <link rel="stylesheet" href="./assets/css/footer.css">
        <title>Profil GIP</title>
    </head>

    <body>
        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>
        </div>

        <section id="profil">
            <?php

            $id = $_SESSION['id'];

            $connexion = getConnexion();
            $requete = $connexion->query(" SELECT * FROM users WHERE id = '$id' ");
            $data = $requete->fetch();

            echo '
    <div class="container justify-content-center profile col-lg-10 ">
        <div class="row offset-lg-2">
            <div class="col-md-4">
                <div class="profile-img">
                    <img  src="' . $data['society_logo'] . '" alt="">
                   
                </div>
            </div>
            <div class="col-md-6">
                <div class="profile-head">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <p class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> Votre Société</p>
                        </li>
                    </ul>
                    <h5>
                        ' . $data['society_name'] . '
                             <button class="float-right" style="border:0px" id="input_societyname"><i class="far fa-edit fa-pull-right"></i></button>
                                    
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_name" name="society_name" style="display:none;" value="' . $data['society_name'] . '">
                                <input type="submit" name="sendForm_societyname" id="sendForm_societyname" style="display:none;" value="Valider">
                                </form>
                                                   
                    </h5>
                    <h6>
                        ' . $data['society_city'] . '
                           <button class="float-right" style="border:0px" id="input_societycity"><i class="far fa-edit fa-pull-right"></i></button>
                                   
                               <form method="POST" action="editprofil.php">           
                               <input type="text" class="form-control" id="society_city" name="society_city" style="display:none;" value="' . $data['society_city'] . '">
                               <input type="submit" name="sendForm_societycity" id="sendForm_societycity" style="display:none;" value="Valider">
                               </form>
                    </h6>
                    <h6>
                        ' . $data['society_address'] . '
                            <button class="float-right" style="border:0px" id="input_societyaddress"><i class="far fa-edit fa-pull-right"></i></button>
                                       
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_address" name="society_address" style="display:none;" value="' . $data['society_address'] . '">
                                <input type="submit" name="sendForm_societyaddress" id="sendForm_societyaddress" style="display:none;" value="Valider">
                                </form>                        
                    </h6>
                    <h6>
                            ' . $data['society_postalcode'] . '
                            <button class="float-right" style="border:0px" id="input_societypostalcode"><i class="far fa-edit fa-pull-right"></i></button>

                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="society_postalcode" name="society_postalcode" style="display:none;" value="' . $data['society_postalcode'] . '">
                                    <input type="submit" name="sendForm_societypostalcode" id="sendForm_societypostalcode" style="display:none;" value="Valider">  
                                    </form>                      
                    </h6>
                    <h6>
                            Siret : ' . $data['society_siret'] . '
                            <button class="float-right" style="border:0px" id="input_societysiret"><i class="far fa-edit fa-pull-right"></i></button>
                                                       
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_siret" name="society_siret" style="display:none;" value="' . $data['society_siret'] . '">
                                <input type="submit" name="sendForm_societysiret" id="sendForm_societysiret" style="display:none;" value="Valider"> 
                                </form>                    
                    </h6>
                    <h6>
                            APE : ' . $data['society_ape'] . '
                            <button class="float-right" style="border:0px" id="input_societyape"><i class="far fa-edit fa-pull-right"></i></button>       
                                           
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_ape" name="society_ape" style="display:none;" value="' . $data['society_ape'] . '">
                                <input type="submit" name="sendForm_societyape" id="sendForm_societyape" style="display:none;" value="Valider">
                                </form>               
                    </h6>
                    <h6>
                            Raison sociale : ' . $data['society_socialreason'] . '
                            <button class="float-right" style="border:0px" id="input_societysocialreason"><i class="far fa-edit fa-pull-right"></i></button>      
                                  
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_socialreason" name="society_socialreason" style="display:none;" value="' . $data['society_socialreason'] . '">
                                <input type="submit" name="sendForm_societysocialreason" id="sendForm_societysocialreason" style="display:none;" value="Valider"> 
                                </form>                      
                    </h6>
                    <h6>
                            Site : ' . $data['society_website'] . '
                            <button class="float-right" style="border:0px" id="input_societywebsite"><i class="far fa-edit fa-pull-right"></i></button>
                                                       
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_website" name="society_website" style="display:none;" value="' . $data['society_website'] . '">
                                <input type="submit" name="sendForm_societywebsite" id="sendForm_societywebsite" style="display:none;" value="Valider">
                                </form>
                    </h6>
                    <h6>
                            Activités : ' . $data['society_activity'] . '
                            <button class="float-right" style="border:0px" id="input_societyactivity"><i class="far fa-edit fa-pull-right"></i></button>
                                        
                                <form method="POST" action="editprofil.php">
                                <input type="text" class="form-control" id="society_activity" name="society_activity" style="display:none;" value="' . $data['society_activity'] . '">
                                <input type="submit" name="sendForm_societyactivity" id="sendForm_societyactivity" style="display:none;" value="Valider">
                                </form>                  
                    </h6>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <p class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Vous</p>
                            </li>
                        </ul>
                </div>
            </div>
        </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row offset-lg-2"">
                            <div class="col-md-6">
                                <label>Nom</label>
                            </div>
                            <div class="col-md-6">
                                ' . $data['name'] . '
                                <button class="float-right" style="border:0px" id="input_name"><i class="far fa-edit fa-pull-right"></i></button>
                                    
                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="name" name="name" style="display:none;" value="' . $data['name'] . '">
                                    <input type="submit" name="sendForm_name" id="sendForm_name" style="display:none;" value="Valider">
                                    </form>                            
                            </div>
                        </div>
                        <div class="row offset-lg-2"">
                            <div class="col-md-6">
                                <label>Prénom</label>
                            </div>
                            <div class="col-md-6">
                                ' . $data['firstname'] . '
                                <button class="float-right" style="border:0px" id="input_firstname"><i class="far fa-edit fa-pull-right"></i></button>
                                      
                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="firstname" name="firstname" style="display:none;" value="' . $data['firstname'] . '">
                                    <input type="submit" name="sendForm_firstname" id="sendForm_firstname" style="display:none;" value="Valider">
                                    </form>
                            </div>
                        </div>
                        <div class="row offset-lg-2"">
                            <div class="col-md-6">
                                <label>Mail</label>
                            </div>
                            <div class="col-md-6">
                                ' . $data['email'] . '
                                    <button class="float-right" style="border:0px" id="input_email" >
                                        <i class="far fa-edit">
                                        </i>
                                    </button>
                                        
                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="email" name="email" style="display:none;" value="' . $data['email'] . '">
                                    <input type="submit" name="sendForm_email" id="sendForm_email" style="display:none;" value="Valider">
                                    </form>
                            </div>
                        </div>
                        <div class="row offset-lg-2"">
                            <div class="col-md-6">
                                <label>Téléphone</label>
                            </div>
                            <div class="col-md-6">
                                ' . $data['phone'] . '
                                <button class="float-right" style="border:0px" id="input_phone"><i class="far fa-edit "></i></button>
                                           
                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="phone" name="phone" style="display:none;" value="' . $data['phone'] . '">
                                    <input type="submit" name="sendForm_phone" id="sendForm_phone" style="display:none;" value="Valider">
                                    </form>
                            </div>
                        </div>
                        <div class="row offset-lg-2"">
                            <div class="col-md-6">
                                <label>Mot de Passe</label>
                            </div>
                            <div class="col-md-6">
                                ********** 
                                <button class="float-right" style="border:0px" id="input_password"><i class="far fa-edit fa-align-right"></i></button>
                                    
                                    <form method="POST" action="editprofil.php">
                                    <input type="text" class="form-control" id="password" name="password" style="display:none;" value="' . $data['password'] . '">
                                    <input type="submit" name="sendForm_password" id="sendForm_password" style="display:none;" value="Valider">
                                    </form>
                                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>         
    </div>
    </form>'
            ?>


        </section>

        <div class="container include">
            <div class="row">
                <?php include('footer.php'); ?>
            </div>
        </div>

        <script type="text/javascript" src="./assets/js/script.js"></script>

    </body>

    </html>

<?php

} else {

    header('location: index.php');

    exit;
}
?>