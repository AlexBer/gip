
<?php

if (!isset($_SESSION)) {
    session_start();
}

?>

<?php
if (isset($_SESSION['id'])){

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/style.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="./assets/fonts/GOTHAM">
    <title>Europe en Hainaut</title>
</head>

<body>
    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); 
            $data = getProjectById($_GET['id']);
            $project = $data->fetch();
            ?>
        </div>
    </div>
    <div class="container ">
        <div class="row justify-content-center">
            <div class="col-lg-6 offset-lg-2">
                <div class="card" style="">
                    <img src="<?php echo $project['picture']; ?>" class="card-img-top" alt="">
                    <div class="card-body">
                        <h5 class="card-title" style="color:blue; font-weight: bold;"><?php echo $project['name']; ?></h5>
                        <p class="card-text"><?php echo $project['description']; ?></p>
                    </div>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Date de publication : <?php echo $project['date_of_posting']; ?></li>
                        <li class="list-group-item">Date de fin :
                            <?php if (isset($project['deadline'])) {
                                echo $project['deadline'];
                            } else {
                                echo "Non défini";
                            } ?></li>
                        <!-- <li class="list-group-item">Vestibulum at eros</li> -->
                    </ul>
                    <div class="card-body justify-content-center">
                        <a href="<?php echo $project['specifications']; ?>" class="btn btn-primary btn-xs active" role="button" aria-pressed="true"><i class="fas fa-file-download"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container include">
        <div class="row">
            <?php include('footer.php'); ?>
        </div>
    </div>
    <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="./assets/js/navbar.js"></script>
</body>

</html>

<?php
} else {
    header('location: index.php');
    exit;
}
?>