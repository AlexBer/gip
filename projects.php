<?php
if (!isset($_SESSION)) {
    session_start();
}
// include('connexionbdd.php');
// require('fonctions.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <title>Espace Administrateur</title>
</head>

<body>

    <body>
        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>
        </div>
        <button class="offset-lg-2" id="addproject"><i class="fas fa-plus"></i></button>
        <a href="admin.php"><button><i class="far fa-user fa-2x mr-3 ml-3">Gestion des roles</i></button></a>
        <div class="container" id="addprojectform" style="display:none;">
            <button id="closeprojectform"><i class="fas fa-times"></i></button>
            <div>
                <form method="POST" action="create_project.php">
            </div>
            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-3 col-sm-8">
                    <label for="name">Nom du projet</label>
                    <input required type="text" class="form-control" id="name" name="name" placeholder="Nom du projet">
                </div>
                <div class="form-group col-lg-3 col-sm-8">
                    <label for="program">Programme du projet</label>
                    <input required type="text" class="form-control" id="program" name="program" placeholder="Programme du projet">
                </div>
            </div>
            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-6 col-sm-8">
                    <label for="description">Description du projet</label>
                    <textarea required class="form-control" id="description" name="description" rows="5"></textarea>
                </div>
            </div>
            <div class="row justify-content-center offset-lg-2">
                <div class="form-group col-lg-3 col-sm-8">
                    <label for="specifications">Cahier des charges</label>
                    <input required type="text" class="form-control" id="specifications" name="specifications" placeholder="Lien du cahier des charges">
                </div>
                <div class="form-group col-lg-3 col-sm-8">
                    <label for="deadline">Date limite</label>
                    <input required type="date" class="form-control" id="deadline" name="deadline">
                </div>
                <?php $categories = getCategories();
                while ($data = $categories->fetch()) { ?>
                    <div class="form-group col-lg-3 col-sm-8">
                        <label for="categorie"><?php echo $data['name']; ?></label>
                        <input type="checkbox" value="<?php echo $data['id']; ?>" class="form-control" id="categories" name="categories[]">
                    </div>
                <?php } ?>
            </div>
            <div class="row justify-content-center offset-lg-2">
                <button type="submit" id="sendForm" class="btn btn-primary center">Créer un projet</button>
            </div>
            </form>
        </div>
        <div class="container" id="listofprojects">
            <div class="row justify-content-center offset-lg-2">
                <div class="table-responsive">
                    <div class="col-lg-12 col-md-10 col-sm-6">
                        <table class="table justify-content-center">
                            <thead>
                                <tr>
                                    <th scope="col justify-content-center">Programme</th>
                                    <th scope="col justify-content-center">Projet</th>
                                    <th scope="col justify-content-center">Date limite</th>
                                    <th scope="col justify-content-center">Gestion des projets</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $req = getProjects();
                                while ($data = $req->fetch()) {; ?>
                                    <tr>
                                        <!-- <th scope="row justify-content-center">1</th> -->
                                        <td><?php echo $data['program']; ?></td>
                                        <td><?php echo $data['name']; ?></td>
                                        <td><?php echo $data['deadline']; ?></td>
                                        <td>
                                            <a href="./projects.php?edit&id=<?php echo $data['id']; ?>" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="far fa-edit"></i></a>
                                            <a href="./projects.php?delete&id=<?php echo $data['id']; ?>" class="btn btn-primary btn-lg active" role="button" aria-pressed="true"><i class="far fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                <?php }
                            if (isset($_GET['delete'])) {
                                deleteProject($_GET['id']);
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (isset($_GET['edit'])) {
            $id = $_GET['id'];
            $data = getProjectById($id);
            $project = $data->fetch();
            // var_dump($project);
            ?>
            <div class="container">
                <div>
                    <form method="POST" action="edit_project.php">
                </div>
                <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $project['id']; ?>">
                <div class="row justify-content-center offset-lg-2">
                    <div class="form-group col-lg-3 col-sm-8">
                        <label for="name">Nom du projet</label>
                        <input required value="<?php echo $project['name']; ?>" type="text" class="form-control" id="name" name="name" placeholder="Nom du projet">
                    </div>
                    <div class="form-group col-lg-3 col-sm-8">
                        <label for="program">Programme du projet</label>
                        <input required value="<?php echo $project['program']; ?>" type="text" class="form-control" id="program" name="program" placeholder="Programme du projet">
                    </div>
                </div>
                <div class="row justify-content-center offset-lg-2">
                    <div class="form-group col-lg-6 col-sm-8">
                        <label for="description">Description du projet</label>
                        <textarea required value="" class="form-control" id="description" name="description" rows="5"><?php echo $project['description']; ?></textarea>
                    </div>
                </div>
                <div class="row justify-content-center offset-lg-2">
                    <div class="form-group col-lg-3 col-sm-8">
                        <label for="specifications">Cahier des charges</label>
                        <input required value="<?php echo $project['specifications']; ?>" type="text" class="form-control" id="specifications" name="specifications" placeholder="Lien du cahier des charges">
                    </div>
                    <div class="form-group col-lg-3 col-sm-8">
                        <label for="deadline">Date limite</label>
                        <input required value="<?php echo $project['deadline']; ?>" type="date" class="form-control" id="deadline" name="deadline">
                    </div>
                </div>
                <div class="row justify-content-center offset-lg-2">
                    <button type="submit" id="sendForm" class="btn btn-primary center">Modifier</button>
                </div>
                </form>
            <?php } ?>
            <div class="container include">
                <div class="row">
                    <?php include('footer.php'); ?>
                </div>
            </div>
            <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
            <script src="./assets/js/admin.js"></script>
    </body>

</html>