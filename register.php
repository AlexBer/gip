<?php
session_start();

// include('connexionbdd.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/css/register.css">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Inscription GIP</title>
</head>

<body>
    <div class="container include">
        <div class="row">
            <?php include('navbar.php'); ?>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center offset-lg-2">
            <small class="form-text text-muted"><a href="login.php"><span class="underline">Vous êtes déjà inscrit ? Connectez-vous.</span></a></small>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <small id="required" class="form-text text-muted mt-3">Les champs marqués d'un <span class="asterisk">*</span> sont obligatoires</small>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <h2>Société : </h2>
        </div>
        <div>
            <form method="POST" action="result_register.php">
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_name"><span class="asterisk">* </span>Nom de la société</label>
                <input required type="text" class="form-control" id="society_name" name="society_name" placeholder="Nom de la société">
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_siret"><span class="asterisk">* </span>N° SIRET</label>
                <input required type="text" class="form-control" id="society_siret" name="society_siret" placeholder="N° SIRET de votre société">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_ape"><span class="asterisk">* </span>Code APE</label>
                <input required type="text" class="form-control" id="society_ape" name="society_ape" placeholder="Code APE">
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="siret"><span class="asterisk">* </span>Raison sociale</label>
                <input required type="text" class="form-control" id="society_socialreason" name="society_socialreason" placeholder="Raison sociale">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_address"><span class="asterisk">* </span>Adresse</label>
                <input required type="text" class="form-control" id="society_address" name="society_address" placeholder="Adresse">
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_postalcode"><span class="asterisk">* </span>Code postal</label>
                <input required type="text" class="form-control" id="society_postalcode" name="society_postalcode" placeholder="Code postal">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_city"><span class="asterisk">* </span>Ville</label>
                <!-- <input required type="text" class="form-control" id="society_city" name="society_city" placeholder="Ville"> -->
                <select required class="form-control" id="society_city" name="society_city">
                </select>
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="society_website">Site WEB</label>
                <input type="text" class="form-control" id="society_website" name="society_website" placeholder="Site WEB">
            </div>
            <input required type="hidden" class="form-control" id="society_logo" name="society_logo" value="./assets/img/logos/logo1.png">
        </div>
        <div class="row justify-content-center offset-lg-2">
            <h2>Référent de la société : </h2>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-8 col-sm-8">
                <label for="name"><span class="asterisk">* </span>Nom</label>
                <input required type="text" class="form-control" id="name" name="name" placeholder="Nom du référent">
            </div>
            <div class="form-group col-lg-8 col-sm-8">
                <label for="firstname"><span class="asterisk">* </span>Prénom</label>
                <input required type="firstname" class="form-control" id="firstname" name="firstname" placeholder="Prénom du référent">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-8 col-sm-8">
                <label for="name"><span class="asterisk">* </span>Numéro de téléphone</label>
                <input required type="text" class="form-control" id="phone" name="phone" placeholder="Téléphone du référent">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8">
                <label for="email"><span class="asterisk">* </span>Adresse E-mail</label>
                <input required type="email" class="form-control" id="email" name="email" placeholder="Votre adresse email">
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="cemail"><span class="asterisk">* </span>Confirmez votre adresse E-mail</label>
                <input required type="cemail" class="form-control" id="cemail" name="cemail" placeholder="Confirmez votre adresse email">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <span class="alert alert-danger" id="emailAlert" role="alert" style="display:none;">Les emails ne correspondent pas</span>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <div class="form-group col-lg-4 col-sm-8 align-self-center">
                <label for="password"><span class="asterisk">* </span>Mot de passe</label>
                <input required type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe">
            </div>
            <div class="form-group col-lg-4 col-sm-8">
                <label for="cpassword"><span class="asterisk">* </span>Confirmez votre mot de passe</label>
                <input required type="password" class="form-control" id="cpassword" name="cpassword" placeholder="Confirmez votre mot de passe">
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2">
            <span class="alert alert-danger" id="passwordAlert" role="alert" style="display:none;">Les mot de passes ne correspondent pas</span>
        </div>
        <div class="row justify-content-center offset-lg-2 ">
            <div class="form-check col-lg-6 col-sm-8 mt-4">
                <input required type="checkbox" class="form-check-input" id="cgu" name="cgu">
                <label class="form-check-label" for="cgu"><span class="asterisk">** </span>J'accepte les <a href="#">conditions générales d'utilisation</a></label>
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2 ">
            <div class="form-check col-lg-6 col-sm-8 mt-4">
                <input type="checkbox" class="form-check-input" id="newsletter" name="newsletter" value="1">
                <label class="form-check-label" for="newsletter">J'accepte de recevoir par email les informations d'Europe en Hainaut</label>
            </div>
        </div>
        <div class="row justify-content-center offset-lg-2 mt-3 mb-4">
            <button type="submit" id="sendForm" class="btn btn-primary center">S'inscrire</button>
            </form>
        </div>
    </div>
    <div class="container include">
        <div class="row">
        <?php include('footer.php'); ?>
        </div>
    </div>
    <script src="./assets/js/ajax.js"></script>
    <script src="./assets/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="./assets/js/register.js"></script>
</body>

</html>