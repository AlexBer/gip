<?php

session_start();
include('connexionbdd.php');

if(isset($_POST['email'])  && isset($_POST['password'])) {
    $connexion= getConnexion();

    $requete="SELECT * FROM users WHERE email=? AND password=? ";
    $result = $connexion->prepare($requete);
    
    $email = $_POST['email'];
    $password = $_POST['password'];

    $result->execute(array($email, $password));
    $data = $result->fetch();
    // var_dump($data);

    if(!empty($data)) {

        $_SESSION['email'] = $data['email'];
        $_SESSION['password'] = $data['password'];
        $_SESSION['id'] = $data['id'];
        $_SESSION['admin'] = $data['admin'];

        $authOK = true;
    }
}

header('location: index.php');
exit;

?>