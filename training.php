<?php
if (!isset($_SESSION)) {
  session_start();
}
?>
<?php
if (isset($_SESSION['id'])){

?>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
    <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="./assets/css/navbar.css">
    <link rel="stylesheet" href="./assets/css/training.css">
    <link rel="stylesheet" href="./assets/css/footer.css">
    <title>Formations GIP</title>
  </head>

  <body>
    <div class="container include">
      <div class="row">
        <?php include('navbar.php'); ?>
      </div>
    </div>

    <section id="trainings">
      <div class="container">
        <div class="row offset-lg-2">
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/ateliers.jpg" alt="ATELIER">
              <div class="overlay">
                <h2>NOS ATELIERS</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. In elit ligula, rhoncus quis quam et, ullamcorper efficitur dui. Curabitur molestie fermentum massa vel commodo. Cras faucibus arcu a rhoncus mollis. </a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/referencement.jpg" alt="REFERENCEMENT">
              <div class="overlay">
                <h2>NOTRE REFERENCEMENT</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. In elit ligula, rhoncus quis quam et, ullamcorper efficitur dui. Curabitur molestie fermentum massa vel commodo. Cras faucibus arcu a rhoncus mollis. </a></p>
              </div>
            </div>
          </div>
        </div>
        <div class="row mt-lg-3 offset-lg-2">
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/tutorat.jpg" alt="TUTORAT">
              <div class="overlay">
                <h2>TUTORAT</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. In elit ligula, rhoncus quis quam et, ullamcorper efficitur dui. Curabitur molestie fermentum massa vel commodo. Cras faucibus arcu a rhoncus mollis.</a></p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-auto col-sm-6 col-xs-6 ">
            <div class="hovereffect">
              <img class="img-responsive" src="./assets/img/club.jpg" alt="CLUB">
              <div class="overlay">
                <h2>NOTRE CLUB</h2>
                <p><a href="#">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed commodo enim nulla, in efficitur ante imperdiet ac. In elit ligula, rhoncus quis quam et, ullamcorper efficitur dui. Curabitur molestie fermentum massa vel commodo. Cras faucibus arcu a rhoncus mollis.</a></p>
              </div>
            </div>
          </div>
        </div>

        <div class="row justify-content-center ">
          <div class="col-lg-8 col-sm-6 col-xs-2 offset-2">
            <div class="googleCalendar">
              <iframe src="https://calendar.google.com/calendar/embed?src=gq521mq65233qirqr3fp2rcdqg%40group.calendar.google.com&ctz=Europe%2FParis" style="border: 0" width="800" height="600" frameborder="0" scrolling="no">
              </iframe>
            </div>
          </div>
        </div>

      </div>
    </section>

    <div class="container include">
      <div class="row">
        <?php include('footer.php'); ?>
      </div>
    </div>
    
    <script type="text/javascript" src="./assets/js/script.js"></script>
  </body>

  </html>
<?php
} else {
  header('location: index.php');
  exit;
}
?>