<?php
if (!isset($_SESSION)) {
    session_start();
}
?>
<?php
if (isset($_SESSION['id'])) {

    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://necolas.github.io/normalize.css/8.0.1/normalize.css">
        <link rel="stylesheet" href="./assets/bootstrap/css/bootstrap.min.css">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="./assets/css/navbar.css">
        <link rel="stylesheet" href="./assets/css/profil.css">
        <link rel="stylesheet" href="./assets/css/footer.css">
        <link rel="stylesheet" href="./assets/css/weshare.css">
        <title>We share GIP</title>
    </head>

    <body>
        <div class="container include">
            <div class="row">
                <?php include('navbar.php'); ?>
            </div>
        </div>
        <section class="weshare">
            <div class="container col-lg-8 offset-lg-2">
                <h1 class="text-center text-uppercase">RECHERCHE DE PARTENARIATS ENTRE PORTEURS DE PROJETS</h1>
                <div class="row">
                    <div class="table-responsive mt-3">
                        <table class="table justify-content-center ">
                            <thead>
                                <tr>
                                    <th scope="col justify-content-center">Porteur de projet</th>
                                    <th scope="col justify-content-center">Programme UE</th>
                                    <th scope="col justify-content-center">Partenariat recherché</th>
                                    <th scope="col justify-content-center">Expérience Fonds UE</th>
                                    <th scope="col justify-content-center">Commentaires</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="col justify-content-center">Porteur de projet</th>
                                    <th scope="col justify-content-center">Programme UE</th>
                                    <th scope="col justify-content-center">Partenariat recherché</th>
                                    <th scope="col justify-content-center">Expérience Fonds UE</th>
                                    <th scope="col justify-content-center">Commentaires</th>
                                </tr>
                                <tr>
                                    <th scope="col justify-content-center">Porteur de projet</th>
                                    <th scope="col justify-content-center">Programme UE</th>
                                    <th scope="col justify-content-center">Partenariat recherché</th>
                                    <th scope="col justify-content-center">Expérience Fonds UE</th>
                                    <th scope="col justify-content-center">Commentaires</th>
                                </tr>
                                <tr>
                                    <th scope="col justify-content-center">Porteur de projet</th>
                                    <th scope="col justify-content-center">Programme UE</th>
                                    <th scope="col justify-content-center">Partenariat recherché</th>
                                    <th scope="col justify-content-center">Expérience Fonds UE</th>
                                    <th scope="col justify-content-center">Commentaires</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="container include">
            <div class="row">
                <?php include('footer.php'); ?>
            </div>
        </div>
        <script type="text/javascript" src="./assets/js/script.js"></script>
    </body>

    </html>
<?php
} else {
    header('location: index.php');
    exit;
}
?>